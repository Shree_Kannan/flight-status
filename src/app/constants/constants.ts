import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { UtilsService } from '../services/utils.service';

@Injectable({
  providedIn: 'root'
})

export class Constants {

    constructor(public utils: UtilsService) {}

    DEFAULT_START_TIME = this.utils.getDateByFormat('YYYY-MM-DDThh:mm:ss') + 'Z';

    DEFAULT_END_TIME = this.utils.getDateByFormat('YYYY-MM-DDT23:59:59', 1) + 'Z';

    FLIGHT_NUMBER_SEARCH_DATE = this.utils.getDateByFormat('YYYYMMDD');

    FLIGHT_STATUS_API_URL = 'https://api.airfranceklm.com/opendata/flightstatus';

    API_KEY = '6f6szh34a953fxjrgs6b6djk';

    BASIC_HEADERS = {
        'accept-language' : 'en-GB',
        // tslint:disable-next-line:object-literal-key-quotes
        'Accept' : 'application/hal+json;version=com.afkl.operationalflight.v3',
        'Api-Key' : this.API_KEY
    };

    HEADERS = {
      headers: new HttpHeaders(this.BASIC_HEADERS)
    };

    lazyLoadChunkThreshold = 10;
}
