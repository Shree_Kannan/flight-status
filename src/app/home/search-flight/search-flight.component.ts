import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FlightsApiService } from '../../services/flights-api.service';
import { UtilsService } from '../../services/utils.service';
import { Constants } from '../../constants/constants';
import { FlightRequestParam, FlightInitialRequestWrap,
  Flights, OperationalFlights, CreateFlightRequest } from '../../interfaces/model';

@Component({
  selector: 'app-search-flight',
  templateUrl: './search-flight.component.html',
  styleUrls: ['./search-flight.component.scss']
})

export class SearchFlightComponent implements OnInit, OnDestroy {

  flightResponse: object;
  showFlightStatusError: boolean;
  errorMsg: string;
  observableObject = null;
  constructor(
    public flightservice: FlightsApiService, public utils: UtilsService,
    public consts: Constants, private changeDetect: ChangeDetectorRef) { }

  ngOnInit() {
  }
  focushandler(flightno: HTMLInputElement, origin: HTMLInputElement, destination: HTMLInputElement) {
    /*
     * simple focus handler to clear error
     * also clear orgin & destination in focused on flightno
     */
    this.errorMsg = '';
    this.showFlightStatusError = false;
    if (flightno) {
      origin.value = '';
      destination.value = '';
    }
  }

  searchFlight(flightno: HTMLInputElement, origin: HTMLInputElement, destination: HTMLInputElement) {
    /*
     * show loading indicator on search button clicked
     */
    const paramValidity = this.validateSearchValues(flightno, origin, destination);
    if (!paramValidity.valid) {
      /**
       *  show corresponsing error msg and return control flow if input has invalid characters
       */
      this.showErrorMessage(paramValidity.msg);
      return false;
    } else {
      /**
       * initially clear all the error msg from screen
       */
      this.hideErrorMessageFromUI();
    }

    let reqParam: string | object;
    let serializeRequired: boolean;
    let requestObject: CreateFlightRequest;
    /*
      function called on search flight button clicked
      get values from respective fields
      serialize request param and call flight status api
     */
    if (flightno && origin.value === '' && flightno.value !== '') {
      /*
        only flight no is provided
       */
      requestObject = {
        flightNumber: flightno.value
      };
      reqParam = this.createRequestParamForFlightSearch(requestObject, true);
      serializeRequired = false;

    } else if (origin && destination &&
      origin.value !== '' || destination.value !== '') {
      /*
       *  only origin or destrination are provided
       */
      flightno.value = '';
      requestObject = {
        origin: origin.value,
        destination: destination.value
      };
      reqParam = this.createRequestParamForFlightSearch(requestObject, false);
      serializeRequired = true;
    } else {
      /*
      show error mesage if any none the field has been entered
       */
      this.showErrorMessage('Enter any value on the field above');
    }
    if (reqParam) {
      this.utils.sendData({ showLoader: true });
      this.observableObject = this.makeFlightStatusAPIrequest({
        flightno,
        origin,
        destination,
        reqParam,
        serializeRequired
      });
    }
  }

  createRequestParamForFlightSearch(flightParam: CreateFlightRequest, isFlightNumberSearch: boolean) {
    /**
     * create request param required to make flight status api call
     * flight number request follow YYYYDDMM_Airlinecode_flightnumber
     * origin and destination accepts current time util tommorrow in an object
     */
    let reqParam: FlightInitialRequestWrap | string;
    const date = this.consts.FLIGHT_NUMBER_SEARCH_DATE;
    if (flightParam && isFlightNumberSearch) {

      reqParam = '/' + date + '+' +
        flightParam.flightNumber.match(/[a-zA-Z]*/)[0].toUpperCase() + '+' +
        flightParam.flightNumber.match(/[0-9]*$/)[0];

    } else if (flightParam && !isFlightNumberSearch) {

      reqParam = {
        startRange: this.consts.DEFAULT_START_TIME,
        endRange: this.consts.DEFAULT_END_TIME,
        origin: flightParam.origin.toUpperCase(),
        destination: flightParam.destination.toUpperCase()
      };

    } else {
      reqParam = null;
    }
    return reqParam;
  }

  makeFlightStatusAPIrequest(options: FlightRequestParam) {
    /*
     * once request param is formed, Make get request to flight status API
     */
    let flightResponseWrap;
    return this.flightservice.getFlightStatusAPI(options.reqParam, options.serializeRequired).
      subscribe((flightResponse: OperationalFlights | Flights) => {
        // tslint:disable-next-line:no-string-literal
        if (flightResponse && !flightResponse['operationalFlights']) {

          flightResponseWrap = {
            operationalFlights: [flightResponse]
          };

          flightResponse = flightResponseWrap;
        }
        this.utils.sendData({ flightResponse, err: null });
      }, err => {
        options.flightno.blur();
        options.origin.blur();
        options.destination.blur();

        this.utils.sendData({ hideLoader: true });
        this.utils.sendData({ data: null, err });

        this.showErrorMessage('Sorry, Searched flight not found. Please try different flight.');

        this.changeDetect.detectChanges();
      });
  }

  hideErrorMessageFromUI(): boolean {
    this.errorMsg = '';
    this.showFlightStatusError = false;
    return true;
  }
  showErrorMessage(msg: string): void {
    this.errorMsg = msg;
    this.showFlightStatusError = true;
  }

  validateSearchValues(flightno: HTMLInputElement, origin: HTMLInputElement, destination: HTMLInputElement) {
    /*
     * XSS validation on input fields
     * users should enter valid characters
     */
    const paramValidity = {
      valid: true,
      msg: ''
    };
    const accesKey = 'value';
    if (flightno && flightno[accesKey] !== '') {

      paramValidity.valid = flightno[accesKey].match(/[A-Za-z]{2}[0-9]{4}$/gi) &&
        flightno[accesKey].match(/[A-Za-z]{2}[0-9]{4}$/gi)[0].length > 0;
      paramValidity.msg = 'Enter valid flight number';

    }
    if (origin && origin[accesKey] !== '') {

      paramValidity.valid = origin[accesKey].match(/[A-Z]{3}(?<![A-Za-z]{4})(?![A-Za-z])/gi) &&
        origin[accesKey].match(/[A-Z]{3}(?<![A-Za-z]{4})(?![A-Za-z])/gi)[0].length > 0;
      paramValidity.msg = 'Enter valid origin code (AMS, LHR, etc)';

    }
    if (destination && destination[accesKey] !== '') {

      paramValidity.valid = destination[accesKey].match(/[A-Z]{3}(?<![A-Za-z]{4})(?![A-Za-z])/gi) &&
        destination[accesKey].match(/[A-Z]{3}(?<![A-Za-z]{4})(?![A-Za-z])/gi)[0].length > 0;
      paramValidity.msg = 'Enter valid destination code (CDG, JFX, etc)';

    }
    return paramValidity;
  }

  ngOnDestroy() {
    if (this.observableObject) {
      this.observableObject.unsubscribe();
    }
  }
}
