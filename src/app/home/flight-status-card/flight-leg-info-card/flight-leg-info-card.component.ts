import { Component, OnInit , Input} from '@angular/core';

@Component({
  selector: 'app-flight-leg-info-card',
  templateUrl: './flight-leg-info-card.component.html',
  styleUrls: ['./flight-leg-info-card.component.scss']
})
export class FlightLegInfoCardComponent implements OnInit {

  flightLegs: any;
  constructor() { }

  @Input() legsInfo: any;

  ngOnInit() {
    this.flightLegs = this.legsInfo;
  }

}
