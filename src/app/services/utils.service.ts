import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import moment from 'moment';
import { OperationalFlights, Flights, FlightLeg } from '../interfaces/model';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  /*
    utility service helper
    holds helper to communicate with-in components
    helper to convert data to specific format
    helper to serialize request params
  */
  bsubject = new BehaviorSubject(null);
  flightsResponse: OperationalFlights;
  constructor() { }

  getData() {
    /*
      creating an observable object to subscribe at anytime
      helps to data from different component
     */
    return this.bsubject.asObservable();
  }
  sendData(data, err?: any) {
    /*
     * creating a data publisher to send data across components
     */
    this.bsubject.next({ data, err });
  }

  serializeRequestParams(reqParams): string {
    /*
     * serialize request object into get reuest query param
     */
    let finalString = '';
    // tslint:disable-next-line:forin
    for (const key in reqParams) {
      if (finalString !== '') {
        finalString += '&';
      }
      finalString += key + '=' + encodeURIComponent(reqParams[key]);
    }
    return '?' + finalString;
  }

  getDateByFormat(format: string, add = 0, date?: string): string {
    /*
     * date formater to convert date into specific format passed
     */
    return date ? moment(date).add(add, 'days').format(format) : moment().add(add, 'days').format(format);
  }

  getDateIntoMilliseconds(date) {
    /*
    convert normal into milliseconds
     */
    return moment(date).valueOf();
  }

  setFlightsData(flightsResponse: OperationalFlights) {
    this.flightsResponse = flightsResponse;
  }

  getFlightsData(): OperationalFlights {
    return this.flightsResponse;
  }

  processFlightDataFromResponse(flights): Array<Flights> {
    const processedFlightResponse = flights.map((flight: Flights) => {
      return this.convertTimeTerminalInformation(flight);
    });
    return this.sortFlightBasedOnDepartureTime(processedFlightResponse);
  }

  convertTimeTerminalInformation(flight): Flights {
    flight.flightLegs = this.processLegInformation(flight.flightLegs).map((leg) => {
      /*
      changing terminal information to N/A  not available
     */
      leg.departureInformation.airport.terminalCode = leg.departureInformation.airport.places ?
        leg.departureInformation.airport.places.terminalCode : '- N/A';

      leg.arrivalInformation.airport.terminalCode = leg.arrivalInformation.airport.places ?
        leg.arrivalInformation.airport.places.arrivalPositionTerminal : '- N/A';

      leg.departureInformation.times.timeInMilliseconds = leg.departureInformation.times.actual ?
        this.getDateIntoMilliseconds(leg.departureInformation.times.actual) :
        this.getDateIntoMilliseconds(leg.departureInformation.times.scheduled);

      leg.arrivalInformation.times.timeInMilliseconds = leg.arrivalInformation.times.actual ?
        this.getDateIntoMilliseconds(leg.arrivalInformation.times.actual) :
        this.getDateIntoMilliseconds(leg.arrivalInformation.times.scheduled);

      /*
        convert YYYY-MM-DDtHH:MM:SSZ to readable time format to its local zone
       */
      leg.departureInformation.times.scheduledDay =
        this.getDateByFormat('DD MMM YYYY', 0, leg.departureInformation.times.scheduled);

      leg.departureInformation.times.actualDay = leg.departureInformation.times.actual ?
        this.getDateByFormat('DD MMM YYYY', 0, leg.departureInformation.times.actual) :
        this.getDateByFormat('DD MMM YYYY', 0, leg.departureInformation.times.scheduled);

      leg.departureInformation.times.scheduledTime =
        this.getDateByFormat('hh:mm:ss A', 0, leg.departureInformation.times.scheduled);

      leg.departureInformation.times.actualTime = leg.departureInformation.times.actual ?
        this.getDateByFormat('hh:mm:ss A', 0, leg.departureInformation.times.actual) :
        this.getDateByFormat('hh:mm:ss A', 0, leg.departureInformation.times.scheduled);

      leg.arrivalInformation.times.scheduledDay =
        this.getDateByFormat('DD MMM YYYY', 0, leg.arrivalInformation.times.scheduled);

      leg.arrivalInformation.times.actualDay = leg.arrivalInformation.times.actual ?
        this.getDateByFormat('DD MMM YYYY', 0, leg.arrivalInformation.times.actual) :
        this.getDateByFormat('DD MMM YYYY', 0, leg.arrivalInformation.times.scheduled);

      leg.arrivalInformation.times.scheduledTime =
        this.getDateByFormat('hh:mm:ss A', 0, leg.arrivalInformation.times.scheduled);

      leg.arrivalInformation.times.actualTime = leg.arrivalInformation.times.actual ?
        this.getDateByFormat('hh:mm:ss A', 0, leg.arrivalInformation.times.actual) :
        this.getDateByFormat('hh:mm:ss A', 0, leg.arrivalInformation.times.scheduled);

      return leg;
    });
    return flight;
  }
  processLegInformation(flightLegs): Array<FlightLeg> {
    /*
    * find the transfer time of the flight
    * find layover by comparing with next arrival and current departure time difference
    */
    return flightLegs.map((currentLeg, i) => {
      if (currentLeg && flightLegs[i + 1] && flightLegs[i + 1]
        .departureInformation.airport.name === currentLeg.arrivalInformation.airport.name) {

        const arrivalTime = currentLeg.arrivalInformation.times.actual ?
          currentLeg.arrivalInformation.times.actual : currentLeg.arrivalInformation.times.scheduled;

        const departureTime = flightLegs[i + 1].departureInformation.times.actual ?
          flightLegs[i + 1].departureInformation.times.actual : flightLegs[i + 1].departureInformation.times.scheduled;

        currentLeg.transitTime = Math.round(moment.duration(moment(departureTime).diff(moment(arrivalTime)).valueOf()).asHours() * 10) / 10;
      }
      return currentLeg;
    });
  }
  sortFlightBasedOnDepartureTime(flights) {
    /*
      sort the final processed data to display flight based on current time util tomo
     */
    return flights.sort((first, second) => {
      return Math.min.apply(Math, first.flightLegs.map(leg => leg.departureInformation.times.timeInMilliseconds)) -
      Math.min.apply(Math, second.flightLegs.map(leg => leg.departureInformation.times.timeInMilliseconds));
    });
  }
}
