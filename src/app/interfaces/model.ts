export interface FlightRequestParam {
    flightno: HTMLInputElement;
    origin: HTMLInputElement;
    destination: HTMLInputElement;
    reqParam: string | object;
    serializeRequired: boolean;
    flightNumber?: string;
}
export interface CreateFlightRequest {
    flightno?: string;
    origin?: string;
    destination?: string;
    flightNumber?: string;
}

export interface FlightInitialRequestWrap {
    startRange: string;
    endRange: string;
    origin: string;
    destination: string;
}

export interface OperationalFlights {
    operationalFlights: Array<Flights>;
}
export interface Flights {
    flightNumber: number;
    flightScheduleDate: string;
    id: string;
    haul: string;
    route: Array<string>;
    airline: Airline;
    codeShareRelations: Array<CodeShareRetions>;
    flightRelations: FlightRelations;
    flightStatusPublic: string;
    flightStatusPublicLangTransl: string;
    flightLegs: Array<FlightLeg>;
    internalStatusArrFocus: string;
}
export interface ObservableData {
    data: CustomObservableType;
}

interface OnwardFlightData {
    id: string;
    flightScheduleDate: string;
    airlineCode: string;
    flightNumber: number;
}

interface CodeShareRetions {
    marketingFlightNumber: number;
    code: string;
    type: string;
    airline: Airline;
}

interface FlightRelations {
    onwardFlightData: OnwardFlightData;
}

interface Airline {
    code: string;
    name: string;
}

export interface FlightLeg {
    status: string;
    statusName: string;
    publishedStatus: string;
    departureInformation: DepartureInformation;
    arrivalInformation: ArrivalInformation;
    legStatusPublic: string;
    legStatusPublicLangTransl: string;
    passengerCustomsStatus: string;
    serviceType: string;
    serviceTypeName: string;
    scheduledFlightDuration: string;
    departureDateTimeDifference: string;
    arrivalDateTimeDifference: string;
    timeToArrival: string;
    completionPercentage: number;
    timeZoneDifference: number;
    aircraft: Aircraft;
    irregularity: Irregularity;
    internalLegStatusArrFocus: boolean;
}
interface Irregularity {
    cancelled: string;
    delayReasonCodePublic: Array<string>;
}
interface Aircraft {
    registration: string;
    typeCode: string;
    typeName: string;
    subFleetCodeId: string;
    ownerAirlineCode: string;
    ownerAirlineName: string;
    physicalPaxConfiguration: string;
    physicalFreightConfiguration: string;
    operationalConfiguration: string;
}
interface DepartureInformation {
    airport: Airport;
    times: DepartureTimes;
}
interface ArrivalInformation {
    airport: Airport;
    times: ArrivalTimes;
}
interface Airport {
    code: string;
    name: string;
    city: City;
    location: Location;
    terminalCode?: string;
    places?: Places;
}
interface City {
    code: string;
    name: string;
    country: Country;
}
interface Location {
    latitude: number;
    longitude: number;
}
interface Places {
    gateNumber?: Array<string>;
    parkingPosition?: string;
    pierCode?: string;
    terminalCode?: string;
    boardingPier?: string;
    checkInZone?: Array<number>;
    boardingContactType?: string;
    parkingPositionType?: string;
    arrivalPositionTerminal?: string;
}
interface DepartureTimes {
    actual: string;
    scheduled: string;
    modified: string;
    latestPublished: string;
    actualTakeOffTime: string;
    scheduledDay?: string;
    scheduledTime?: string;
    actualTime?: string;
    actualDay?: string;
    timeInMilliseconds?: number;
}
interface ArrivalTimes {
    actual: string;
    scheduled: string;
    modified: string;
    latestPublished: string;
    actualTouchDownTime: string;
    estimated?: object;
    scheduledDay?: string;
    scheduledTime?: string;
    actualTime?: string;
    actualDay?: string;
    timeInMilliseconds?: number;
}
interface Country {
    areaCode: string;
    code: string;
    name: string;
    euroCountry: string;
    euCountry: string;
}

interface CustomObservableType {
    showLoader: boolean;
    hideLoader: boolean;
    err: object;
    flightData: OperationalFlights;
}
